import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import se.svt.caspar.amcp.AmcpChannel;
import se.svt.caspar.amcp.AmcpLayer;
import se.svt.caspar.producer.Video;

public class Command {

	static byte PLAY = 0;
	static byte LOAD = 1;
	static byte STOP = 2;
	static byte LOOP = 3;
	static byte PAUSE = 4;
	static byte RESUME = 5;
	
	private int layer;
	private byte action;
	private String name;
	
	public Command(int layer, byte action, String name) {
		this.layer = layer;
		this.action = action;
		this.name = name;
	}
	
	public Command(int layer, byte action) {
		this.layer = layer;
		this.action = action;
	}
	
	public String getName() {
		return name;
	}
	
	public int getLayer() {
		return layer;
	}
	
	public int getAction() {
		return action;
	}
	
	public static byte lookupAction(String s) {
		switch(s) {
		case "PLAY":
			return PLAY;
		case "LOAD":
			return LOAD;
		case "STOP":
			return STOP;
		case "LOOP":
			return LOOP;
		case "PAUSE":
			return PAUSE;
		case "RESUME":
			return RESUME;
		}
		return -1;
	}
	
	public static void execute(Command c, ConcurrentHashMap<Integer,LayerInfo> currentLayer, AmcpChannel channel,
			CopyOnWriteArrayList<Trigger> activeTriggers) {
		AmcpLayer layer = new AmcpLayer(channel, c.getLayer());
		if(c.getAction() == Command.PLAY) {
			layer.play();
		}
		else if(c.getAction() == Command.LOAD) {
			layer.loadBg(new Video(c.getName()));
			currentLayer.put(layer.layerId(), new LayerInfo(c.getName()));
		}
		else if(c.getAction() == Command.STOP) {
			layer.stop();
		}
		else if(c.getAction() == Command.PAUSE) {
			layer.pause();
		}
		else if(c.getAction() == Command.RESUME) {
			layer.sendCommand("RESUME");
		}
		else if(c.getAction() == Command.LOOP) {
			layer.play();
			layer.loadBg(new Video(currentLayer.get(layer.layerId()).getName()));
			currentLayer.put(layer.layerId(), currentLayer.get(layer.layerId()));
			Trigger t = new Trigger(Trigger.END, c.getLayer());
			t.setLoop();
			t.addCommand(new Command(c.getLayer(), Command.LOOP, currentLayer.get(layer.layerId()).getName()));
			activeTriggers.add(t);
		}
		else {
			System.err.println("Invalid command");
		}
	}
	
	public String toString() {
		return "COMMAND: " + getLayer() + " " + getAction() + " " + getName();
	}
	
}
